# mongodb-cgi-mar-2-2020

# List of software to be installed
The list of software to be installed before training starts can be found [here](./mongodb-training-list-of-software-to-be-installed.pdf)

# Exercises
These are some [exercise problems](./data/03-restaurants/). These will be good practice on different types of queries.

# Chapters and Topics

## MongoDB basics
- RDBMS vs NoSql DBs
- Types of NoSQL DBs
- Pros and Cons of NoSQL DBs
- Document data model
- Introduction to MongoDB
- Features of MongoDB
- Installation and getting started 
- Using Robo 3T 

## Mongo Shell 
- Getting started
- Databases, collections, and documents
- CRUD operations – Inserts, queries, updates, deletes
- Creating and querying with indexes - Indexing and explain()
- Basic administration
 
## Using MongoDB in Node.js 
- Quick introduction to Node.js 
- Using MongoDB in Node.js via the Mongo DB driver 
- Introduction to the Mongoose ODM 
- CRUD operations – Inserts, queries, updates, deletes 
- Installing Mongoose and using it 
- Schemas and Models 
- Operation Hooks 
- Data Validation during operations 
- Building a simple application 

## Schema Design for Document-oriented Data
- Principles of schema design
- Designing an e-commerce Schema
 
## Constructing Queries 
- Getting Started
- Query Criteria and Selectors
- Overview of the aggregation framework
 
## Document Updates, Atomic Operations, and Deletes 
- Introduction to document updates
- Modify by replacement vs Modify by operator
- Update operators
- The findAndModify command
- Deletes
 
## Indexing and Query Optimization 
- Core concepts 
- Index types 
- Index administration 
- Query optimization
 
## Replication 
- Introduction to replication 
- Replica sets and setup 
- Verifying replication and failover 
- Connections 
  
## Sharding 
- Introduction to sharding 
- When to shard 
- Components of a sharded cluster 
- Ways to shard
- Building a shard 
- Strategies for distributing data 
 
## Deployment and Administration 
- Hardware and provisioning 
- Deployment environment 
- Provisioning 
- Monitoring and diagnostics 
- Logging 
- MongoDB diagnostic tools and commands 

# References
- [Documentation](docs.mongodb.com)
- [Training from MongoDB](university.mongodb.com)
- [MongoDB for .NET developers](https://university.mongodb.com/courses/M101N/about)
- [MongoDB driver for C# and .NET](https://docs.mongodb.com/ecosystem/drivers/csharp/)
- [MongoDB driver for Node.js](https://mongodb.github.io/node-mongodb-native/?jmp=docs)
- [Mongoose ODM for Node.js apps](https://mongoosejs.com/)
- [Schema Design - Part 1](https://www.mongodb.com/blog/post/6-rules-of-thumb-for-mongodb-schema-design-part-1)
- [Schema Design - Part 2](https://www.mongodb.com/blog/post/6-rules-of-thumb-for-mongodb-schema-design-part-2)
- [Schema Design - Part 3](https://www.mongodb.com/blog/post/6-rules-of-thumb-for-mongodb-schema-design-part-3-)
- [Comparison of different NoSQL DBs under various workloads - with different read-write ratios](https://jaxenter.com/evaluating-nosql-performance-which-database-is-right-for-your-data-107481.html)