let x = "hello";

function sum( x, y ) {
    return x + y;
}

sum( 12, 13 ); // internally 12 and 13 are float values (64 bit storage)

// object literal syntax
let john = {
    name : 'John',
    age : 32,
    address : {
        street: '#32, Rhenius St',
        pinCode: 560001,
        area: 'Shantinagar'
    },
    emailids: [
        'john@gmail.com',
        123,
        {
            contact: 'john@cgi.com',
            type: 'work'
        }
    ]
}