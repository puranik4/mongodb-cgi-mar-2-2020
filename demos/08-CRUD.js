use cgiDB;

// insert() [deprecated], insertOne(), insertMany()
db.participants.insertOne(
    {
        _id: 1,
        name: 'Geeta',
        age: 32,
        address : {
            street: '#32, Rhenius St',
            pinCode: 560001,
            area: 'Shantinagar'
        },
        emailids: [
            'john@gmail.com',
            123,
            {
                contact: 'john@cgi.com',
                type: 'work'
            }
        ]
    }
);

db.participants.insertOne(
    {
        _id: 2,
        name: 'Rahima',
        age: 32,
        address : {
            street: '#32, Rhenius St',
            pinCode: 560001,
            area: 'Shantinagar'
        },
        emailids: [
            'john@gmail.com',
            123,
            {
                contact: 'john@cgi.com',
                type: 'work'
            }
        ]
    }
);

db.participants.insertOne(
    {
        name: 'John',
        address : {
            street: '#32, Rhenius St',
            pinCode: 560001,
            area: 'Shantinagar'
        },
        emailids: [
            'john@gmail.com',
            123,
            {
                contact: 'john@cgi.com',
                type: 'work'
            }
        ]
    }
);

db.participants.insertOne(
    {
        name: 'Jane',
        age: 28,
        spouse: ObjectId("5e5ce6af3261b3dcb9ec5ea7"),
        address : {
            street: '#32, Rhenius St',
            pinCode: 560001,
            area: 'Shantinagar'
        },
        emailids: [
            'john@gmail.com',
            123,
            {
                contact: 'john@cgi.com',
                type: 'work'
            }
        ]
    }
);

// try using insert() - works, but deprecated
db.participants.insertMany(
    [
        {
            name: 'David',
            age: 40
        },
        {
            name: 'Diana',
            age: 45
        }
    ]
);

db.participants.find();

// filtering while retrieving ("WHERE" clause)
// we pass a document to find()
db.participants.find(
    { 
        age: 28
    }
);

// $gt, $gte, $lte, $ne
db.participants.find(
    {
        age : {
            $lt: 35
        }
    }
);

db.participants.find(
    {
        age : {
            $gt: 30,
            $lt: 35
        },
        name: new RegExp( "^R" )
    }
);

// age and name MUST meet the requirements
db.participants.find(
    {
        age : {
            $gt: 30,
            $lt: 35
        },
        name: /^R/
    }
);

db.participants.save(
    {
        _id: { x: 101 },
        name: 'Mark Doe'
    }
);

db.participants.save(
    {
        name: 'Mark Smith'
    }
);

// SELECT <field 1>, <field 2> (projection)
// include some fields only (whitelisting fields we want)
db.participants.find( { }, { _id: 0, name : 1, age: 1 } );

// exclude some fields (and include all the rest)
db.participants.find( { }, { name : 0, age: 0 } );

// trying both exclude and include - wont work
db.participants.find( { }, { _id: 0, address: 0, name : 1, age: 1 } );

// age or name MUST meet the requirement
db.participants.find(
    {
        $or: [
            {
                age : {
                    $gte: 28,
                    $lte: 35
                }
            },
            {
                name: /^R/
            }
        ]
    }
);

db.participants.find(
    {
        $and: [
            {
                age : {
                    $gte: 28,
                    $lte: 35
                }
            },
            {
                name: /^R/
            }
        ]
    }
);

// filtering based on subdocument fields
// no match - this is not how we search for field match in subdocument
db.participants.find(
    {
        address: {
            area: 'Shantinagar'
        }
    }
);

// searching match on entire subdocument
db.participants.find(
    {
        address: {
            "street" : "#32, Rhenius St",
            "pinCode" : 560001,
            "area" : "Shantinagar"
        }
    }
);

// match for address.area
db.participants.find(
    {
        "address.area" : 'Shantinagar'
    }
);

// insert 25000 documents
for( let i = 1; i <= 25000; i++ ) {
    db.stocks.insertOne({
        _id: i,
        price: Math.floor( Math.random() * 1000 )
    });
}

// find returns a cursor
let cursor = db.participants.find()

// hasNext() is a cursor method to check if more docs exist
cursor.hasNext(); // true
cursor.next(); // gets next document and increments the cursor pointer

db.participants.findOne(); // returns the first match - not a cursor

// ---- UPDATES ----
// 1. which document to update (same like find filter clause) - only first matched document is updated by default
// 2. how to modify the document (what all should we update) - modify or replace is decided by how we form this argument
// 3. options to update - example, we can specify that all documents matched should be updated

// 2 types of updates - modify and replace

// replace update is possible with update() but not with updateOne(), updateMany()
db.participants.update(
    {
        name: 'Geeta'
    },
    {
        name: 'Geeta Gopal',
        age: 32
    }
);

// modify update
db.participants.updateOne(
    {
        name: 'Rahima Malik'
    },
    {
        $set: {
            name: 'Rahima Noor Malik',
            age: 32,
            worksFor: 'BCD Consulting',
            role: 'Consultant'
        },
        $unset: {
            "address.area": 1
        }
    }
);

db.participants.updateOne(
    {
        name: 'Rahima Noor Malik'
    },
    {
        $set: {
            age: NumberInt( 32 )
        }
    }
);

db.participants.deleteOne(
    {
        name: 'David'
    }
);